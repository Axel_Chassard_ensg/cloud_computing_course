# Question 1 

Pour déployer l'application sans utiliser la CLI d'heroku, j'ai commencé par créer une application dans le dashboard d'Heroku. Ensuite j'ai fait un fork du projet findmefast sur mon compte github, j'ai relié ce compte à Heroku, puis je suis allé dans l'onglet "Deploy" et j'ai choisi les options Automatic Deploy et j'ai sélectionné le bouton Deploy Branch dans la partie Manuel Deploy. 

# Question 2 

Heroku offre un PaaS, c'est à dire qu'il a probablement souscrit à une offre de IaaS et l'additionne d'un environnement pour déployer l'application, c'est à dire qu'il offre un système d'exploitation avec node.js et npm d'installer sur lequel il peut effectuer les commandes npm install et npm run start mais cette fois-ci l'application n'est pas déployée localement mais sur heroku disponible sur internet. 