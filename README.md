# Cloud Computing course

Ce dépôt regroupe mes réponses aux différents TP réalisés lors du cour de cloud computing.

Chaque répertoire contient un fichier .md comprenants les réponses aux questions des diapositives.

Bonne lecture. 

# Auteur

Axel CHASSARD - étudiant à l'ENSG-Géomatique, 3ème année de cycle ingénieur, spécialité TSI - axel.chassard@ensg.eu