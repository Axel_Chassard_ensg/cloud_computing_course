# Question 1

Les temps réels sont plus long avec l'utilisation de docker par rapport à l'utilisation sur l'ordinateur directement. Le temps est encore plus long avec l'émulateur. 

# Question 2 

On suppose que real est le temps complet du calcul des nombres premiers, de a à z, user est le temps de calcul du côté de lespace utilisateur à l'extérieur du noyau et sys est le temps de calcul du processus dans le noyau.

C'est trois temps sont différents car l'un d'entre eux calcul le temps dans le noyau, un autre calcul le temps hors noyau et le dernier calcul le temps complet.

La partie sys est la plus faible car elle est souvent très rapide car proche de la machine. 

Pour la compilation directement sur le système, il n'y a pas de virtualisation c'est pourquoi le temps real correspond à la somme du temps user et du temps sys. 

Pour l'utilisation de docker, le temps user est court car il s'agit uniquement du temps de lancement de la commande docker. Par contre, le temps real est plus long car il englobe le temps de calcul des nombres premiers à l'intérieur du container. 

Enfin, pour l'utilisation avec l'émulateur, le temps user est cette fois-ci proche du temps réel car il n'y a pas d'isolement de l'espace utilisateur, et le temps est long car il y a d'abord une compilation effectuée pour le processeur ARM de l'émulateur debian puis ensuite une compilation pour le processeur x86 de la machine hôte. 

# Question 3 

Le temps user pour la partie avec Docker est petit car il s'agit uniquement du temps de traitement du CPU pour lancer la commande Docker. Tout ce qui se passe dans le container pour calculer les nombres premiers n'est pas pris en compte car l'espace utilisateur du container est isolé.

# Question 4

Pour obtenir le temps user effectif dans le conteneur, on peut lancer le conteneur avec un bash interactif et effectuer cette fois la commande time. commande : 

    docker run -it prime bash

puis dans le bash interactif ouvert : 

    time ./prime 10000

