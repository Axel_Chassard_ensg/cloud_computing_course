# Question 1

Le logiciel Wordpress est installé en client lourd, il n'y a pas d'utilisation de cloud. Ainsi, pour l'utiliser il faut déjà avoir la capacité en terme de ressource machine et de stockage de donnée. Les applications, les données, sont présents en local et non pas hébergé sur un serveur. De plus, le site n'est pas encore publier sur internet, il n'est qu'en local tandis que wordpress.com permet d'héberger le site sur internet. 

# Question 2 

Il faudrait commencé par se doter d'un service de PaaS, puis se baser du code source de WordPress pour proposer des nouveaux contenus, comme par exemple proposer de la création interactive de site web mais avec une composante cartographique plus forte. Ensuite, il faudrait proposer ces améliorations en offre cloud en tant que SaaS.

# Question 3 

On utilise tout l'environnement mis à disposition par le PaaS afin d'éditer notre code pour le WordPress cartographique et héberger la base de données pour les données qu'entreront les utilisateurs. 

Ensuite, on créé un site web intermédiaire qui constitura une interface entre nos services et les utilisateurs. A la manière de wordpress.com, les utilisateurs s'y connecteront et pourront utiliser nos services à partir de celui-ci.

![Architecture proposée](Archi_cours_cloud_computing.png)